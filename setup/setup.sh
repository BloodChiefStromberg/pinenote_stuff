sh setup_env.sh

# install packages
apt install omz zsh tmux git

# Get configs
cd ../ # ARS_DEVEL
git clone https://gitlab.com/BloodChiefStromberg/ars_configurations.git

# setup tmux
cd ars_configurations
ln -s $ARS_DEVEL/.tmux.conf /home/$ARS_USERNAME/
mkdir -p ~/.tmux/plugins/tpm
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
