#!/bin/bash
brcm=/lib/firmware/brcm/

mkdir -p $ARS_PN_STUFF/pinenote_stuff/setup/bt
currdir=$PWD
cd $ARS_DEVEL/setup/bt

wget https://github.com/LibreELEC/brcmfmac_sdio-firmware/raw/master/BCM4345C0.hcd
wget https://github.com/LibreELEC/brcmfmac_sdio-firmware/raw/master/brcmfmac43455-sdio.bin
wget https://github.com/LibreELEC/brcmfmac_sdio-firmware/raw/master/brcmfmac43455-sdio.txt

sudo cp BCM4345C0.hcd $brcm/
sudo cp brcmfmac43455-sdio.bin $brcm/brcmfmac43455-sdio.pine64,pinenote-v1.2.bin
sudo cp brcmfmac43455-sdio.txt $brcm/brcmfmac43455-sdio.pine64,pinenote-v1.2.txt

cd $currdir
